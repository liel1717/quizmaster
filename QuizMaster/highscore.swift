//
//  highscore.swift
//  QuizMaster
//
//  Created by Jerry Lindh on 2018-11-12.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation
import UIKit

class highscore: NSObject, NSCoding{
    var name: String = ""
    var score: Int = 0
    var image = UIImage(named: "index")
    
    init(_ name: String, _ score: Int, _ image: UIImage){
        self.name = name
        self.score = score
        self.image = image
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(score, forKey: "score")
        aCoder.encode(image, forKey: "image")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let score = aDecoder.decodeInteger(forKey: "score")
        let image = aDecoder.decodeObject(forKey: "image") as? UIImage ?? UIImage(named: "index")
        self.init(name, score, image!)
    }
}
