//
//  ProfileViewController.swift
//  wireframes
//
//  Created by Johan Kåhlman on 2018-11-02.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet var profileView: UIView!
    @IBOutlet weak var currentName: UILabel!
    @IBOutlet weak var changenameTextField: UITextField!
    @IBOutlet weak var changenameButton: UIButton!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var updatePhotoButton: UIButton!
    
    @objc func checkAction(_ sender : UITapGestureRecognizer) {
        dismissKeyboard()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.checkAction(_:)))
        self.profileView.addGestureRecognizer(gesture)
        
        // MARK: Use this to reset the locally saved highscores
        //Storagehandler.removeObject(named: "easyHS")
        //Storagehandler.removeObject(named: "mediumHS")
        //Storagehandler.removeObject(named: "hardHS")
        
        self.ProfileImage.layer.cornerRadius = 50
        self.ProfileImage.clipsToBounds = true;
        self.ProfileImage.image = Storagehandler.getProfileImage()
        self.currentName.text = Storagehandler.getName()
    }
    
    @IBAction func changeName(_ sender: Any) {
        setName()
    }
    
    func setName() {
        DataHandler.instance.setName(self.changenameTextField.text!)
        currentName.text = changenameTextField.text
        dismissKeyboard()
    }
    
    // MARK: Adding camera and photolibrary functionallity
    @IBAction func cameraButtonAction(_ sender: UIButton) {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func updatePhoto(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.editedImage] as? UIImage
        ProfileImage.image = image
        Storagehandler.setProfileImage(image!)
        DataHandler.instance.setImage(image!)

        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
