//
//  RestHandler.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-23.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation

class RestHandler {
    
    // MARK: Fetching API
    static func getQuestions(_ difficulty: String, completion: ((Bool) -> Void)?) {
        
        let API_PATH: String = "https://opentdb.com/api.php?amount=15&difficulty=\(difficulty)&type=multiple"
        
        guard let url = URL(string: API_PATH ) else {
            completion?(false)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            let decoder = JSONDecoder()
            if let data = responseData, let result = try?
                
                decoder.decode(QuestionsResponse.self, from: data) {
                
                if (difficulty == "easy") {
                    DataHandler.instance.easyQuestions = result.results
                }
                else if (difficulty == "medium") {
                    DataHandler.instance.mediumQuestions = result.results
                }
                else {
                    DataHandler.instance.hardQuestions = result.results
                }
                
                completion?(true)
            } else {
                completion?(false)
            }
        }
        task.resume()
    }
}

