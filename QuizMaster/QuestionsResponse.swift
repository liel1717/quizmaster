//
//  QuestionsResponse.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-23.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation

struct QuestionsResponse: Codable {
    var results: [Question]
}
