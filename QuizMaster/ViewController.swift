//
//  ViewController.swift
//  wireframes
//
//  Created by Johan Kåhlman on 2018-11-02.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Logo Animation
    @IBOutlet weak var imageView: UIImageView!
    var images: [UIImage] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        images = [
            UIImage(named: "logo (1)"),
            UIImage(named: "logo (2)"),
            UIImage(named: "logo (3)"),
            UIImage(named: "logo (4)"),
            UIImage(named: "logo (5)"),
            UIImage(named: "logo (6)"),
            UIImage(named: "logo (7)"),
            UIImage(named: "logo (8)"),
            UIImage(named: "logo (9)"),
            UIImage(named: "logo (10)"),
            UIImage(named: "logo (11)"),
            UIImage(named: "logo (12)"),
            UIImage(named: "logo (13)"),
            UIImage(named: "logo (14)"),
            UIImage(named: "logo (15)"),
            UIImage(named: "logo (16)"),
            UIImage(named: "logo (17)"),
            UIImage(named: "logo (18)"),
            UIImage(named: "logo (19)")
            ] as! [UIImage]
        imageView.animationImages = images
        imageView.animationDuration = 3.0
        imageView.startAnimating()
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        // MARK: Fetching API
        if (DataHandler.instance.easyQuestions.count == 0) {
            RestHandler.getQuestions("easy") { (success) in
            }
            while(DataHandler.instance.easyQuestions.count < 15) {
                usleep(20000)
            }
        }
        if (DataHandler.instance.mediumQuestions.count == 0) {
            RestHandler.getQuestions("medium") { (success) in
            }
            while(DataHandler.instance.mediumQuestions.count < 15) {
                usleep(20000)
            }
        }
        if (DataHandler.instance.hardQuestions.count == 0) {
            RestHandler.getQuestions("hard") { (success) in
            }
            while(DataHandler.instance.hardQuestions.count < 15) {
                usleep(20000)
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        // MARK: Fetching highscores
        DataHandler.instance.easyHighscore = Storagehandler.getEasyHighscore()
        DataHandler.instance.mediumHighscore = Storagehandler.getMediumHighscore()
        DataHandler.instance.hardHighscore = Storagehandler.getHardHighscore()
    }
}






