//
//  highscoreDetailViewController.swift
//  QuizMaster
//
//  Created by Jerry Lindh on 2018-11-12.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class highscoreDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var highscoreNavBar: UINavigationBar!
    
    var highscores: [highscore] = [highscore("", 0, UIImage())]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: The tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(highscores.count < 10){
            return highscores.count
        }
        else {return 10}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.nameLabel.text = highscores[indexPath.row].name
        cell.scoreLabel.text = String(highscores[indexPath.row].score)
        cell.imageLabel.image = highscores[indexPath.row].image
        cell.contentMode = UIView.ContentMode.scaleAspectFit
        cell.imageLabel.clipsToBounds = true
        
        return cell
    }
}
