//
//  QuestionTEST.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-23.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation

import Foundation

class Question: Codable {
    
    var correct_answer: String
    var incorrect_answers: [String]
    var difficulty: String
    var question: String
    
    private enum CodingKeys: String, CodingKey {
        case correct_answer
        case question
        case difficulty
        case incorrect_answers
    }
    
    init(correct_answer: String = "", question: String = "", difficulty: String = "", incorrect_answers: [String] = []) {
        self.correct_answer = correct_answer
        self.question = question
        self.difficulty = difficulty
        self.incorrect_answers = incorrect_answers        
    }
}
