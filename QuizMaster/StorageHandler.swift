//
//  StorageHandler.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-23.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation
import UIKit

class Storagehandler {
    
    static func getName() -> String {
        return UserDefaults.standard.string(forKey: "name") ?? ""
    }
    
    static func setName(_ name: String) {
        UserDefaults.standard.set(name, forKey: "name")
    }
    
    static func setProfileImage(_ image: UIImage){
        UserDefaults.standard.setImage(image: image, forKey: "profileImage")
    }
    static func getProfileImage() ->UIImage {
        if let Image = UserDefaults.standard.imageForKey(key: "profileImage"){
            return Image
        }
        else {
            let Dimage = UIImage(named: "index")
            return Dimage!
        }
    }
    
    static func getEasyHighscore() -> [highscore]{
        let userDefaults = UserDefaults.standard
        if let decoded = userDefaults.object(forKey: "easyHS"){
            let decodedHS = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data) as! [highscore]
            return decodedHS
        }
        else{
            return []
        }
    }
    
    static func setEasyHighscore(_ HS: [highscore]){
        let defaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: HS)
        defaults.set(encodedData, forKey: "easyHS")
        defaults.synchronize()
    }
    
    static func getMediumHighscore() -> [highscore]{
        let userDefaults = UserDefaults.standard
        if let decoded = userDefaults.object(forKey: "mediumHS"){
            let decodedHS = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data) as! [highscore]
            return decodedHS
        }
        else{
            return []
        }
    }
    
    static func setMediumHighscore(_ HS: [highscore]){
        let defaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: HS)
        defaults.set(encodedData, forKey: "mediumHS")
        defaults.synchronize()
    }
    
    static func getHardHighscore() -> [highscore]{
        let userDefaults = UserDefaults.standard
        if let decoded = userDefaults.object(forKey: "hardHS"){
            let decodedHS = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data) as! [highscore]
            return decodedHS
        }
        else{
            return []
        }
    }
    
    static func setHardHighscore(_ HS: [highscore]){
        let defaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: HS)
        defaults.set(encodedData, forKey: "hardHS")
        defaults.synchronize()
    }
    static func removeObject(named: String){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: named)
    }
}

extension UserDefaults {
    func imageForKey(key: String) -> UIImage? {
        var image: UIImage?
        if let imageData = data(forKey: key) {
            image = NSKeyedUnarchiver.unarchiveObject(with: imageData) as? UIImage
        }
        return image
    }
    
    func setImage(image: UIImage?, forKey key: String) {
        var imageData: NSData?
        if let image = image {
            imageData = NSKeyedArchiver.archivedData(withRootObject: image) as NSData?
        }
        set(imageData, forKey: key)
    }
}
