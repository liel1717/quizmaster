//
//  Datahandler.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-23.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import Foundation
import UIKit

class DataHandler {
    static let instance = DataHandler()
    
    var profileName: String?
    var profileImage = UIImage(named: "index")
    
    var questions: [Question] = []
    var easyQuestions: [Question] = []
    var mediumQuestions: [Question] = []
    var hardQuestions: [Question] = []
    
    var difficulty: String = ""
    
    var easyHighscore:[highscore] = []
    var mediumHighscore:[highscore] = []
    var hardHighscore:[highscore] = []
    
    var highscorePlacement = 0
    var endScore = 0
    
    func getName() -> String{
        return Storagehandler.getName()
    }
    
    func isName(_ name: String) -> Bool {
        if profileName == nil {
            profileName = Storagehandler.getName()
        }
        return profileName! == name
    }
    
    func setName(_ name: String) {
        profileName = name
        Storagehandler.setName(name)
    }
    
    func getImage() -> UIImage{
        return profileImage!
    }
    func setImage(_ image: UIImage){
        profileImage = image
    }
    
    func setDifficulty(_ _difficulty: String) {
        difficulty = _difficulty
    }
    
    func setEasyHighscore(_ HS: [highscore]){
        self.easyHighscore = HS
        
        Storagehandler.setEasyHighscore(HS)
    }
    
    func setMediumHighscore(_ HS: [highscore]){
        self.mediumHighscore = HS
        
        Storagehandler.setMediumHighscore(HS)
    }
    
    func setHardHighscore(_ HS: [highscore]){
        self.hardHighscore = HS
        
        Storagehandler.setHardHighscore(HS)
    }
}
