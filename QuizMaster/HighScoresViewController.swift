//
//  HighScoresViewController.swift
//  wireframes
//
//  Created by Johan Kåhlman on 2018-11-02.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class HighScoresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: Decides which highscores to display
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? highscoreDetailViewController {
            if let indexPath = sender as? IndexPath {
                if indexPath.row == 0{
                    destination.highscores =  Storagehandler.getEasyHighscore().sorted(by: { $0.score > $1.score })
                }
                if indexPath.row == 1{
                    destination.highscores = Storagehandler.getMediumHighscore().sorted(by: { $0.score > $1.score })
                }
                if indexPath.row == 2{
                    destination.highscores = Storagehandler.getHardHighscore().sorted(by: { $0.score > $1.score })
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    // MARK: table View for highscores
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        if indexPath.row == 0{
            cell.difficultyLabel.text = "Easy"
        }
        if indexPath.row == 1{
            cell.difficultyLabel.text = "Medium"
        }
        if indexPath.row == 2{
            cell.difficultyLabel.text = "Hard"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);      performSegue(withIdentifier: "highscoreSegue", sender: indexPath)
    }
}
