//
//  ResultViewController.swift
//  QuizMaster
//
//  Created by Jerry Lindh on 2018-11-12.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var highscorePlacementLabel: UILabel!
    
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scoreLabel.text = "Your score: \(DataHandler.instance.endScore)"
        highscorePlacementLabel.text = "Your highscore placement: \(DataHandler.instance.highscorePlacement)"
        
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
}
