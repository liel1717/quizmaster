//
//  PlayViewController.swift
//  wireframes
//
//  Created by Johan Kåhlman on 2018-11-02.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class PlayViewController: UIViewController {
    
    @IBOutlet weak var answer1: UIButton!
    @IBOutlet weak var answer2: UIButton!
    @IBOutlet weak var answer3: UIButton!
    @IBOutlet weak var answer4: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    
    var difficulty = ""
    var questions: [Question] = []
    var correctAnswer: String = ""
    var answeredQuestions = 0
    let numberOfQuestions = 15
    var buttonArray: [UIButton] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: Set difficulty and insert questions accordingly
        self.difficulty = DataHandler.instance.difficulty
        
        if (self.difficulty == "easy") {
            self.questions = DataHandler.instance.easyQuestions
            DataHandler.instance.easyQuestions = []
        }
        else if (self.difficulty == "medium") {
            self.questions = DataHandler.instance.mediumQuestions
            DataHandler.instance.mediumQuestions = []
        }
        else {
            self.questions = DataHandler.instance.hardQuestions
            DataHandler.instance.hardQuestions = []
        }
        
        self.buttonArray = [answer1, answer2, answer3, answer4]
        
        changeQuestions()
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    // MARK: changes buttontext
    func changeQuestions(){
        let random = Int.random(in: 0 ... questions.count-1)
        
        correctAnswer = questions[random].correct_answer
        buttonArray.shuffle()
        questionLabel.text = self.questions[random].question.stringByDecodingHTMLEntities
        
        buttonArray[0].setTitle(self.questions[random].incorrect_answers[0].stringByDecodingHTMLEntities, for: UIControl.State.normal)
        buttonArray[1].setTitle(self.questions[random].incorrect_answers[1].stringByDecodingHTMLEntities, for: UIControl.State.normal)
        buttonArray[2].setTitle(self.questions[random].incorrect_answers[2].stringByDecodingHTMLEntities, for: UIControl.State.normal)
        buttonArray[3].setTitle(self.questions[random].correct_answer.stringByDecodingHTMLEntities, for: UIControl.State.normal)
        
        questions.remove(at: random)
    }
    
    // MARK: Add to highscore functions
    func addtoHighScoreEasy( HS: highscore){
        DataHandler.instance.easyHighscore.append(HS)
    }
    func addtoHighScoreMedium( HS: highscore){
        DataHandler.instance.mediumHighscore.append(HS)
    }
    func addtoHighScoreHard( HS: highscore){
        DataHandler.instance.hardHighscore.append(HS)
    }
    
    func disableButtons(){
        answer4.isEnabled = false
        answer3.isEnabled = false
        answer2.isEnabled = false
        answer1.isEnabled = false
    }
    func enableButtons(){
        answer4.isEnabled = true
        answer3.isEnabled = true
        answer2.isEnabled = true
        answer1.isEnabled = true
    }
    
    // MARK: Function that will be executed after button animation
    func endOfAnimation(){
        if (self.answeredQuestions == self.numberOfQuestions){
            self.endOfQuestions()
            let ResultViewController = self.storyboard?.instantiateViewController(withIdentifier: "results") as! ResultViewController
            self.navigationController?.pushViewController(ResultViewController, animated: true)
        }
    }
    
    @IBAction func answer1Clicked(_ sender: UIButton) {
        checkifCorrect(button: answer1)
    }
    @IBAction func answer2Clicked(_ sender: Any) {
        checkifCorrect(button: answer2)
    }
    @IBAction func answer3Clicked(_ sender: Any) {
        checkifCorrect(button: answer3)
    }
    @IBAction func answer4Clicked(_ sender: Any) {
        checkifCorrect(button: answer4)
    }
    
    func animateCorrect(button: UIButton){
        UIView.animate(withDuration: 1, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations:{ button.backgroundColor =  UIColor.green.withAlphaComponent(0.5)
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.3){
                button.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
            }
        }
        )
    }
    
    // MARK: Function take the pressed button and checks if its correct button
    @objc func checkifCorrect(button: UIButton){
        disableButtons()
        self.answeredQuestions += 1
        if (button.titleLabel?.text == correctAnswer){
            UIView.animate(withDuration: 1, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations:{ button.backgroundColor =  UIColor.green.withAlphaComponent(0.5)
            }, completion: {(finished:Bool) in
                // When the animation is finished
                self.endOfAnimation()
                sleep(1)
                UIView.animate(withDuration: 0.3){
                    button.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
                }
                self.enableButtons()
                if(self.answeredQuestions < self.numberOfQuestions){
                    self.changeQuestions()
                }
            }
            )
            DataHandler.instance.endScore+=1
        }
        else {
            UIView.animate(withDuration: 1, animations:{ button.backgroundColor =  UIColor.red.withAlphaComponent(0.5)}, completion: {(finished:Bool) in
                // End of animation 1
                self.endOfAnimation()
                sleep(1)
                UIView.animate(withDuration: 0.3){
                    button.backgroundColor = UIColor.blue.withAlphaComponent(0.5)
                }
                self.enableButtons()
                if(self.answeredQuestions < self.numberOfQuestions){
                    self.changeQuestions()
                }
            })
            if (answer1.titleLabel?.text == correctAnswer){
                animateCorrect(button: answer1)
            }
            if (answer2.titleLabel?.text == correctAnswer){
                animateCorrect(button: answer2)
            }
            if (answer3.titleLabel?.text == correctAnswer){
                animateCorrect(button: answer3)
            }
            if (answer4.titleLabel?.text == correctAnswer){
                animateCorrect(button: answer4)
            }
        }
    }
    
    // MARK: When all the questions has been answered
    func endOfQuestions(){
        let Player: highscore = highscore(Storagehandler.getName(), DataHandler.instance.endScore, Storagehandler.getProfileImage())
        
        if(difficulty == "easy"){
            addtoHighScoreEasy(HS: Player)
            DataHandler.instance.setEasyHighscore(DataHandler.instance.easyHighscore)
        }
        if(difficulty == "medium"){
            addtoHighScoreMedium(HS: Player)
            DataHandler.instance.setMediumHighscore(DataHandler.instance.mediumHighscore)
        }
        if(difficulty == "hard"){
            addtoHighScoreHard(HS: Player)
            DataHandler.instance.setHardHighscore(DataHandler.instance.hardHighscore)
        }
        
        // MARK: Add the player to one of the highscores
        var place = 1
        if(difficulty == "easy"){
            let highscoreArr =  DataHandler.instance.easyHighscore.sorted(by: { $0.score > $1.score })
            for i in 0...DataHandler.instance.easyHighscore.count-1{
                if(DataHandler.instance.endScore < highscoreArr[i].score){
                    place+=1
                }
            }
        }
        if(difficulty == "medium"){
            let highscoreArr =  DataHandler.instance.mediumHighscore.sorted(by: { $0.score > $1.score })
            for i in 0...DataHandler.instance.mediumHighscore.count-1{
                if(DataHandler.instance.endScore < highscoreArr[i].score){
                    place+=1
                }
            }
        }
        if(difficulty == "hard"){
            let highscoreArr =  DataHandler.instance.hardHighscore.sorted(by: { $0.score > $1.score })
            for i in 0...DataHandler.instance.hardHighscore.count-1{
                if(DataHandler.instance.endScore < highscoreArr[i].score){
                    place+=1
                }
            }
        }
        DataHandler.instance.highscorePlacement = place
    }
}

// MARK: This will escape html characters
private let characterEntities : [ Substring : Character ] = [
    // XML predefined entities:
    "&amp;" : "&",
    "&quot;" : "\"",
    "&#x27;" : "'",
    "&#39;" : "'",
    "&#x92;": "'",
    "&#x96;" : "-",
    "&gt;" : ">",
    "&lt;" : "<",
    "&deg;" : "°",
    "&shy;" : "-",
    "&sup2;" : "²",
    "&euml;" : "ë",
    "&eacute;" : "é",
    "&micro;" : "µ",
    "&ograve;" : "ò",
    "&uuml;" : "ü",
    "&auml;" : "ä",
    "&aring;" : "å",
    "&prime;" : "′",
    "&Prime;" : "″",
    "&Delta;" : "Δ",
    "&ouml;" : "ö",
    "&ntilde;" : "ñ",
    "&aacute;" : "á"
]
extension String {
    
    var stringByDecodingHTMLEntities : String {
        
        func decodeNumeric(_ string : Substring, base : Int) -> Character? {
            guard let code = UInt32(string, radix: base),
                let uniScalar = UnicodeScalar(code) else { return nil }
            return Character(uniScalar)
        }
        
        func decode(_ entity : Substring) -> Character? {
            
            if entity.hasPrefix("&#x") || entity.hasPrefix("&#X") {
                return decodeNumeric(entity.dropFirst(3).dropLast(), base: 16)
            } else if entity.hasPrefix("&#") {
                return decodeNumeric(entity.dropFirst(2).dropLast(), base: 10)
            } else {
                return characterEntities[entity]
            }
        }
        
        var result = ""
        var position = startIndex
        
        // Find the next '&' and copy the characters preceding it to `result`:
        while let ampRange = self[position...].range(of: "&") {
            result.append(contentsOf: self[position ..< ampRange.lowerBound])
            position = ampRange.lowerBound
            
            // Find the next ';' and copy everything from '&' to ';' into `entity`
            guard let semiRange = self[position...].range(of: ";") else {
                // No matching ';'.
                break
            }
            let entity = self[position ..< semiRange.upperBound]
            position = semiRange.upperBound
            
            if let decoded = decode(entity) {
                // Replace by decoded character:
                result.append(decoded)
            } else {
                // Invalid entity, copy verbatim:
                result.append(contentsOf: entity)
            }
        }
        // Copy remaining characters to `result`:
        result.append(contentsOf: self[position...])
        return result
    }
}
