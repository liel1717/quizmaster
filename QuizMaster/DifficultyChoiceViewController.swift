//
//  DifficultyChoiceViewController.swift
//  QuizMaster
//
//  Created by Johan Kåhlman on 2018-11-12.
//  Copyright © 2018 Johan Kåhlman. All rights reserved.
//

import UIKit

class DifficultyChoiceViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataHandler.instance.endScore = 0
        if(DataHandler.instance.getName().count == 0){
            let ResultViewController = self.storyboard?.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
            self.navigationController?.pushViewController(ResultViewController, animated: true)
        }
    }
    
    func setDifficulty(_ difficulty: String) {
        DataHandler.instance.difficulty = difficulty
    }
    
    @IBAction func EasyChoice(_ sender: Any) {
        setDifficulty("easy")
    }
    
    @IBAction func MediumChoice(_ sender: Any) {
        setDifficulty("medium")
    }
    
    @IBAction func HardChoice(_ sender: Any) {
        setDifficulty("hard")
    }
}
